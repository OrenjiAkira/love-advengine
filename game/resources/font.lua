
--[[ DEPENDENCIES ]]--
local ASSETS  = require 'assets'
local RESDATA = require 'resdata'
local ResourceElement = require 'engine.common.resourceelement'

local graphics = love.graphics

--[[ LOCAL VARIABLES ]]--
local _fonts = {}


--[[ LOADER CLASS ]]--

local FontLoader = ResourceElement:new()

function FontLoader:init(specname)
  self:construct('font')
  local filename = self:getFromSpec('filename')
  local size = self:getFromSpec('size')
  local lineheight = self:getFromSpec('lh')
  print(filename, size)
  local font = ASSETS.load('font', filename, size)
  font:setLineHeight(lineheight)
  self.font = font
end

function FontLoader:getAttr(attr, ...)
  return self.font["get"..attr](self.font, ...)
end

function FontLoader:setAsCurrent()
  graphics.setFont(self.font)
end

function FontLoader:getFont()
  return self.font
end

--[[ FONT LOADER MODULE ]]--
local FONT_LOADER = {}

function FONT_LOADER.load(specname)
  local font = _fonts[specname]
  if not font then
    font = FontLoader:new(specname)
    _fonts[specname] = font
  end
  return font
end

return FONT_LOADER

