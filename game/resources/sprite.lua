
--[[ DEPENDENCIES ]]--
local ASSETS  = require 'assets'
local RESDATA = require 'resdata'
local ResourceElement = require 'engine.common.resourceelement'

local floor = math.floor
local min = math.min
local graphics = love.graphics

--[[ LOCAL VARIABLES ]]--
local _animtextures = {}
local _animations = {}

local function _loadAnimation(specname, iw, ih)
  local animation = _animations[specname]
  if not animation then
    animation = {}

    local spec = RESDATA:loadSpec('sprite', specname)
    local frames = spec.frames
    local framecount = #spec.frames
    local rows, cols = unpack(spec.divisions)
    local qw, qh = floor(iw/cols), floor(ih/rows)
    local quads = {}

    for i = 0, rows-1 do
      for j = 0, cols-1 do
        table.insert(quads, graphics.newQuad(j*qw, i*qh, qw, qh, iw, ih))
      end
    end

    for i = 1, framecount do
      local frameinfo = frames[i]
      animation[i] = {
        time = frameinfo.time,
        quad = quads[frameinfo.qidx],
      }
    end
  end
  return animation
end

local function _loadTexture(filename)
  local tex = _animtextures[filename] or ASSETS.load('img', filename)
  _animtextures[filename] = tex
  return tex
end

--[[ LOADER CLASS ]]--
local SpriteLoader = ResourceElement:new()

function SpriteLoader:init(specname)
  self:construct('sprite')
  local filename = self:getFromSpec('filename')
  local texture = _loadTexture(filename)
  local iw, ih = texture:getDimensions()
  local animation = _loadAnimation(specname, iw, ih)
  local rows, cols = unpack(self:getFromSpec('divisions'))
  local qw, qh = floor(iw/cols), floor(ih/rows)

  self.texture = texture
  self.animation = animation
  self.quad_dim = {qw, qh}
  self.current_frame = 1
  self.time = 0
  self.done = false
end

function SpriteLoader:refresh()
  _animations[self.specname] = nil
  self:init(self.specname)
end

function SpriteLoader:getCurrentQuad()
  return (self.animation[self.current_frame] or table.empty).quad
end

function SpriteLoader:getQuadDimensions()
  return unpack(self.quad_dim)
end

function SpriteLoader:getTexture()
  return self.texture
end

function SpriteLoader:update(dt)
  if self.done then return end
  local framecount = #self.animation
  local frame = self.current_frame
  local time = floor(self.time + dt*1000)
  local loop = self:getFromSpec('loop')
  if time >= self.animation[frame].time then
    time = 0
    if loop then frame = frame % framecount + 1
    else frame = min(frame + 1, framecount) end
  end
  self.done = not loop and frame == framecount
  self.current_frame = frame
  self.time = time
end

function SpriteLoader:draw(x, y, r, sx, sy)
  local quad = self:getCurrentQuad()
  local ox, oy = unpack(self:getFromSpec('offset'))
  return graphics.draw(self.texture, quad, x, y, r, sx, sy, ox, oy)
end

--[[ SPRITE LOADER MODULE ]]--
local SPRITE_LOADER = {}

function SPRITE_LOADER.load(specname)
  return SpriteLoader:new(specname)
end

return SPRITE_LOADER

