
--[[ DEPENDENCIES ]]--
local RESDATA = require 'resdata'
local LOADERS = require 'engine.common.modulepack' 'resources'

--[[ MODULE METHODS ]]--
local RESLOADER = {}

local _loaded

function RESLOADER.init()
  _loaded = {}
  for _,spectype in RESDATA:eachSpectype() do
    _loaded[spectype] = {}
  end
end

function RESLOADER.load(restype, specname)
  local resource = _loaded[restype][specname]
  if not resource then
    resource = LOADERS[restype].load(specname, assetpath)
    _loaded[restype][specname] = resource
  end
  return resource
end

return RESLOADER

