
--[[ DEPENDENCIES ]]--
local ASSETS  = require 'assets'
local RESDATA = require 'resdata'
local ResourceElement = require 'engine.common.resourceelement'

local floor = math.floor


--[[ LOCAL VARIABLES ]]--
local _tilesets = {}


--[[ LOADER CLASS ]]--

local TileSetLoader = ResourceElement:new()

function TileSetLoader:init(specname)
  self:construct('tileset')
  local g = love.graphics

  local filename = self:getFromSpec('filename')
  self.texture = ASSETS.load('img', filename)

  local mg = self:getFromSpec('has_safety_margin') and 1 or 0
  local rows, cols = unpack(self:getFromSpec('divisions'))
  local iw, ih = self.texture:getDimensions()
  local qw, qh = floor(iw/cols) - 2*mg, floor(ih/rows) - 2*mg
  local quads = {}
  for i = 0, rows-1 do
    for j = 0, cols-1 do
      local q = g.newQuad(mg+j*(2*mg+qw), mg+i*(2*mg+qh), qw, qh, iw, ih)
      table.insert(quads, q)
    end
  end
  self.quad_dim = {qw, qh}
  self.quads = quads
end

function TileSetLoader:getTexture()
  return self.texture
end

function TileSetLoader:getQuad(idx)
  return self.quads[idx]
end

function TileSetLoader:getQuadDimensions()
  return unpack(self.quad_dim)
end

function TileSetLoader:getQuadUV(idx)
  idx = idx - 1 -- because it starts on 1 instead of 0
  local rows, cols = unpack(self:getFromSpec('divisions'))
  local qw, qh = self:getQuadDimensions()
  local mg = self:getFromSpec('has_safety_margin') and 1 or 0
  local u = mg + (idx % cols) * (qh + 2*mg)
  local v = mg + floor(idx / cols) * (qw + 2*mg)
  return u, v
end

function TileSetLoader:drawOneTile(idx, x, y, ...)
  local q = self.quads[idx]
  local tex = self.texture
  if not q then return end
  return love.graphics.draw(tex, q, x, y, ...)
end


--[[ TILESET LOADER MODULE ]]--
local TILESET_LOADER = {}

function TILESET_LOADER.load(specname)
  local tileset = _tilesets[specname]
  if not tileset then
    tileset = TileSetLoader:new(specname)
    _tilesets[specname] = tileset
  end
  return tileset
end

return TILESET_LOADER

