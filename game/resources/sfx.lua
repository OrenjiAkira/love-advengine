
--[[ DEPENDENCIES ]]--
local ASSETS  = require 'assets'
local RESDATA = require 'resdata'
local ResourceElement = require 'engine.common.resourceelement'

local floor = math.floor


--[[ CONSTANTS ]]--
local _BUFFER_SIZE = 16

--[[ LOCAL VARIABLES ]]--
local _sfxs = {}


--[[ LOADER CLASS ]]--

local SFXLoader = ResourceElement:new()

function SFXLoader:init(specname)
  self:construct('sfx')
  local g = love.graphics
  local filename = self:getFromSpec('filename')
  local volume = self:getFromSpec('volume')
  local pitch = self:getFromSpec('pitch')

  local buffer = {}
  for i = 1, _BUFFER_SIZE do
    local src = ASSETS.load('sfx', filename)
    src:setVolume(volume)
    src:setPitch(pitch)
    buffer[i] = src
  end

  self.idx = 1
  self.buffer = buffer
end

function SFXLoader:play()
  local idx = self.idx
  local src = self.buffer[idx]
  idx = idx % _BUFFER_SIZE + 1
  self.idx = idx
  src:setRelative(0, 0, 0)
  return src:play()
end

function SFXLoader:playRelativeTo(x, y)
  local idx = self.idx
  local src = self.buffer[idx]
  idx = idx % _BUFFER_SIZE + 1
  self.idx = idx
  src:setRelative(x, y, 0)
  return src:play()
end

function SFXLoader:pause()
  for _,src in ipairs(self.buffer) do
    src:pause()
  end
end

function SFXLoader:resume()
  for _,src in ipairs(self.buffer) do
    src:resume()
  end
end

function SFXLoader:stop()
  for _,src in ipairs(self.buffer) do
    src:stop()
  end
end

--[[ SFX LOADER MODULE ]]--
local SFX_LOADER = {}

function SFX_LOADER.load(specname)
  local sfx = _sfxs[specname]
  if not sfx then
    sfx = SFXLoader:new(specname)
    _sfxs[specname] = sfx
  end
  return sfx
end

return SFX_LOADER

