
--[[ DEPENDENCIES ]]--
local ASSETS  = require 'assets'
local RESDATA = require 'resdata'
local ResourceElement = require 'engine.common.resourceelement'
local TIMER = require 'engine.common.timer'

local min = math.min
local max = math.max


--[[ LOCAL VARIABLES ]]--
local _bgms = {}


--[[ LOADER CLASS ]]--
local BGMLoader = ResourceElement:new()

function BGMLoader:init(specname)
  self:construct('bgm')
  local g = love.graphics
  local filename = self:getFromSpec('filename')
  local volume = self:getFromSpec('volume')
  local pitch = self:getFromSpec('pitch')
  local src = ASSETS.load('bgm', filename)
  src:setVolume(volume)
  src:setPitch(pitch)
  src:setLooping(true)
  self.src = src
end

function BGMLoader:fadeIn(secs)
  local maxvol = self:getFromSpec('volume')
  local step = maxvol / secs
  self:setVolume(0)
  self:play()
  TIMER:during(secs, function()
                 local vol = self:getVolume()
                 return self:setVolume(min(maxvol, vol+step))
               end,
               function()
                 return self:setVolume(maxvol)
               end
  )
end

function BGMLoader:fadeOut(secs)
  local step = self:getFromSpec('volume') / secs
  TIMER:during(secs, function()
                 local vol = self:getVolume()
                 return self:setVolume(max(0, vol-step))
               end,
               function()
                 return self:stop()
               end
  )
end

function BGMLoader:__index(key)
  local src = rawget(self, 'src')
  local method = (src or table.empty)[key]

  if method and isfunction(method) then
    return function (_, ...)
      return method(src, ...)
    end
  else
    return getmetatable(self)[key]
  end
end


--[[ BGM LOADER MODULE ]]--
local BGM_LOADER = {}

function BGM_LOADER.load(specname)
  local bgm = _bgms[specname]
  if not bgm then
    bgm = BGMLoader:new(specname)
    _bgms[specname] = bgm
  end
  return bgm
end

return BGM_LOADER

