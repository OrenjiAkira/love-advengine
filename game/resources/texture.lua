
--[[ DEPENDENCIES ]]--
local ASSETS  = require 'assets'
local RESDATA = require 'resdata'
local ResourceElement = require 'engine.common.resourceelement'

local floor = math.floor


--[[ LOCAL VARIABLES ]]--
local _textures = {}


--[[ LOADER CLASS ]]--

local TextureLoader = ResourceElement:new()

function TextureLoader:init(specname)
  self:construct('texture')
  local g = love.graphics
  local filename = self:getFromSpec('filename')
  self.texture = ASSETS.load('img', filename)
end

function TextureLoader:getTexture()
  return self.texture
end

function TextureLoader:drawTexture(x, y, ...)
  return love.graphics.draw(self.texture, x, y, ...)
end

--[[ TEXTURE LOADER MODULE ]]--
local TEXTURE_LOADER = {}

function TEXTURE_LOADER.load(specname)
  local texture = _textures[specname]
  if not texture then
    texture = TextureLoader:new(specname)
    _textures[specname] = texture
  end
  return texture
end

return TEXTURE_LOADER

