
--[[ DEPENDENCIES ]]--
local Visual = require 'engine.common.visual'

--[[ MAP VIEW MODULE ]]--
local View = Visual:new()

function View:init(adventure)
end

function View:draw(g)
end

return View
