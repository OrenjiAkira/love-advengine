
--[[ REQUIRED MODULES ]]--
local Prototype = require 'engine.common.prototype'

--[[ START VIEW CLASS ]]--
local View = Prototype:new()

function View:init()
end

function View:update(dt)
end

function View:draw(g)
end

return View

