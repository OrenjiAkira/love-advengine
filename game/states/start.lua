
--[[ GAME START MENU ]]--

--[[ DEPENDENCIES ]]--
local INPUT = require 'engine.input'
local StartView = require 'view.start'

--[[ STATE LOCAL VARIABLES ]]--
local _view
local _lock

--[[ STATE METHODS ]]--
local state = {}

function state.enter()
end

function state.leave()
end

function state.update(dt)
  return {"SWITCH", GS.play}
end

return state

