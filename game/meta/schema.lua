
return {
  {
    id = 'id', name = "Game ID", type = 'string',
    help = "Used for the name of the save directory.\n"
    .."Changing this may make you lose your save files."
  },
  {
    id = 'title', name = "Game Title", type = 'string',
    help = "Game window's title."
  },
  {
    id = 'display', name = "Display Settings", type = 'group',
    schema = {
      {
        id = 'dimensions', name = "Window Dimensions", type = 'vector',
        range = {64, 3200}, numtype = 'integer',
        signature = {'width', 'height'}, help = "Game window dimensions.",
      },
      {
        id = 'mode', name = "Window Options", type = 'group',
        schema = {
          {
            id = 'vsync', name = "VSync", type = 'boolean',
            help = "Prevent screen tearing.",
          },
          {
            id = 'fullscreen', name = "Fullscreen", type = 'boolean',
            help = "Set fullscreen.",
          },
          {
            id = 'fullscreentype', name = "Fullscreen Type", type = 'enum',
            options = {'desktop', 'exclusive'},
            help = "Set fullscreen type.",
          },
        },
      },
    },
  },
  {
    id = 'graphics', name = "Graphics", type = 'group',
    schema = {
      {
        id = 'tile-dimensions', name = "Tile Dimensions", type = 'vector',
        range = {1}, numtype = 'integer', default = 64, signature = {'w', 'h'},
        help = "Map tile size width dimension."
      },
      {
        id = 'default-filter', name = "Default Filter", type = 'enum',
        options = {'linear', 'nearest'},
        help = "Default rendering filter when scaling textures."
      },
    },
  },
  {
    id = 'gamedata', name = "Game Data", type = 'group',
    schema = {
      {
        id = 'currentmap', name = "First Map", type = 'enum',
        options = 'gamedata.map',
        help = "Set starting map."
      },
      {
        id = 'player', name = "Player Info", type = 'group',
        schema = {
          {
            id = 'specname', name = "Body Type", type = 'enum',
            options = 'gamedata.body',
            help = "Set player's body type."
          },
          {
            id = 'pos', name = "Start Position", type = 'vector',
            size = 2, range = {1}, default = 1, numtype = 'float',
            help = "Set player's start position in map."
          }
        },
      },
    },
  },
}
