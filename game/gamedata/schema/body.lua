
return {
  {
    id = 'speed', name = "Speed", type = 'integer', range = {1,8},
    help = "Base moving speed",
  },
  {
    id = 'shape', name = "Shape", type = 'enum',
    options = {'circle', 'rectangle'},
    help = "Collision shape",
  },
  { id = 'solid', name = "Solidness", type = 'integer', range = {0,8},
    default = 1,
    help = "The higher the solidness, the less of a 'pushover' the body is.\n"
           .."Bodies with higher solidness push less solid bodies around "
           .."when moving.",
  },
  { id = 'geom', name = "Geometry", type = 'group', no_collapse = true,
    schema = {
      { id = 'radius', name = "Radius", type = 'float', range = {0.125, 1},
        default = 0.5,
      },
      { id = 'width', name = "Width", type = 'float', range = {0.25, 2},
        default = 1,
      },
      { id = 'height', name = "Height", type = 'float', range = {0.25, 2},
        default = 1,
      }
    },
    help = "Collision geometry info.",
  },
  { id = 'appearance', name = "Appearance", type = 'array',
    schema = {
      { id = 'name', name = "Name", type = 'string',
        help = "",
      },
      { id = 'sprite', name = "Animated Sprite", type = 'enum',
        options = "resdata.sprite",
        help = "",
      },
    },
    help = "All the animations a body can have.",
  },
}

