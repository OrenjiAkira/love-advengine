
local DB = require 'game.engine.common.db'

local _BASEDIR = 'gamedata'
local _SPECTYPES = {
  'map', 'body', 'scene',
}

return DB:new(_BASEDIR, _SPECTYPES)

