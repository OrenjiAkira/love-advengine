
--[[ LUA EXPANSIONS ]]--

--[[ PRINT ]]--

local echo = print

function print(...)
  if DEVMODE then echo(...) end
  return ...
end

function printf(s, ...)
  return print(string.format(s, ...))
end


--[[ MATH ]]--

local math = math

function math.round(n)
  return math.floor(n+.5)
end

function math.logn(n, b)
  return math.log(n)/math.log(b)
end


--[[ TABLE ]]--

local table = table

table.empty = setmetatable({}, { __newindex = function(self,k,v) end })
table.unpack = unpack or table.unpack

function table.pack(...)
  return {n = select("#", ...), ...}
end

function table.find(t, val)
  for k,v in ipairs(t) do
    if v == val then return k end
  end
  return nil
end


--[[ STRING ]]--

local string = string

strf = string.format
function string.lines(s)
  if s:sub(-1)~="\n" then s=s.."\n" end
  return s:gmatch("(.-)\n")
end

--[[ FUNCTIONS ]]--

local _TYPE_NUMBER   = 'number'
local _TYPE_STRING   = 'string'
local _TYPE_FUNCTION = 'function'
local _TYPE_THREAD   = 'thread'
local _TYPE_TABLE    = 'table'
local _TYPE_USRDATA  = 'userdata'

function __NOTHING__() end
function __IDENTITY__(...) return ... end
function isnumber(v) return type(v) == _TYPE_NUMBER end
function isstring(v) return type(v) == _TYPE_STRING end
function istable(v) return type(v) == _TYPE_TABLE end
function isboolean(v) return v == true or v == false end
function isfunction(v) return type(v) == _TYPE_FUNCTION end
function iscoroutine(v) return type(v) == _TYPE_THREAD end
function isuserdata(v) return type(v) == _TYPE_USRDATA end

