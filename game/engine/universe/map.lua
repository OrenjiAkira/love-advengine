
--[[ MAP MODULE ]]--


--[[ DEPENDENCIES ]]--
local RESDATA     = require 'resdata'
local GameElement = require 'engine.common.gameelement'
local Body        = require 'engine.universe.body'


--[[ MAP CONSTRUCTION ]]--
local Map = GameElement:new()

function Map:init(specname)
  self:construct("map", specname)
end


--[[ SAVE & LOAD ]]--
function Map:load(data)
end

function Map:save()
  local data = {}
  return data
end

function Map:generateTiles(raw)
  local tiles = {}
  local i = 1
  for line in raw:lines() do
    tiles[i] = {}
    for j = 1, #line do
      tiles[i][j] = tonumber(line:sub(j, j))
    end
    i = i + 1
  end
  return tiles
end

--[[ INFO METHODS ]]--
function Map:getInfo()
  return self:getFromSpec("info")
end

function Map:getWidth()
  return self:getInfo()["width"]
end

function Map:getHeight()
  return self:getInfo()["height"]
end

function Map:getDimensions()
  return self:getWidth(), self:getHeight()
end

function Map:getTileset()
  local tileset_specname = self:getFromSpec('tileset')
  return RESDATA.loadSpec("tileset", tileset_specname)
end


--[[ TILES METHODS ]]--
function Map:getTilemap()
  return self.tiles
end

--[[ MAP UPDATE ]]--
function Map:update(dt)
end

--[[ END ]]--
return Map

