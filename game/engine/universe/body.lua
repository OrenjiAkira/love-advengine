
local GameElement = require 'engine.common.gameelement'
local Body = GameElement:new()

function Body:init(specname)
  self:construct("body", specname)
end

--[[ SAVE & LOAD ]]--
function Body:load(data)
end

function Body:save()
  local data = {}
  return data
end

--[[ METHODS ]]--
function Body:getOffset()
  return unpack(self:getFromSpec("offset"))
end

function Body:getSpeed()
  return self:getFromSpec("speed")
end

function Body:getRadius()
  return self:getFromSpec("geom")['radius']
         or (self:getWidth()+self:getHeight()) / 4
end

function Body:getWidth()
  return self:getFromSpec("geom")['width'] or self:getRadius() * 2
end

function Body:getHeight()
  return self:getFromSpec("geom")['height'] or self:getRadius() * 2
end

return Body

