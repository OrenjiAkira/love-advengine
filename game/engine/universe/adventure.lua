
local GameElement = require 'engine.common.gameelement'
local IDGEN = require 'engine.common.idgen'
local Map = require 'engine.universe.map'

local Adventure = GameElement:new()

function Adventure:init(data)
  self.maps = {}
  self.currentmap = false
  self.playtime = 0

  -- loads your game!
  self:load(data)
end

--[[ SAVE & LOAD ]]--
function Adventure:load(data)
  -- load unique id generator
  local id_generator = IDGEN(data.next_id)
  self.idgen = id_generator
  self:setID(id_generator:generate())

  -- load time
  self.playtime = data.playtime or self.playtime

  -- load maps
  self:setCurrentMap(data.currentmap)
  for specname, map in pairs(data.maps) do
    local loaded_map = Map:new(specname)
    loaded_map:load(map, id_generator)    
    self.maps[specname] = loaded_map
  end

  -- set player!
  -- ???
end

function Adventure:save()
  local data = {}

  -- save time
  data.playtime = self.playtime

  -- save maps
  data.currentmap = self.currentmap
  for specname, map in pairs(self.maps) do
    data.maps[specname] = map:save()
  end

  return data
end

--[[ MAP METHODS ]]--
function Adventure:setCurrentMap(specname)
  self.currentmap = specname
  if not self.maps[specname] then
    self.maps[specname] = Map:new(specname)
  end
end

function Adventure:getCurrentMap()
  return self.maps[self.currentmap]
end

--[[ UPDATE ]]--
function Adventure:update(dt)
  local map = self:getCurrentMap()
  return map:update(dt)
end

return Adventure

