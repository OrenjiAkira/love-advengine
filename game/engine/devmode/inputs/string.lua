
local IMGUI = require 'imgui'

local _EMPTY_STRING = ""

return function(target, key)
  local id = key.id
  local default = key.default or _EMPTY_STRING

  target[id] = target[id] or default

  local changed, new = IMGUI.InputText(_EMPTY_STRING, target[id], 64)
  if changed then
    target[id] = new
  end
end
