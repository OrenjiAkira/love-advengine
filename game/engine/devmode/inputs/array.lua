
local IMGUI = require 'imgui'

-- to avoid cross reference
local INPUTSPATH = 'engine.devmode.inputs'
local INPUTS = package.loaded[INPUTSPATH] or require(INPUTSPATH)

local _EMPTY_STRING = ""

return function(target, key)
  local id = key.id
  local name = key.name or _EMPTY_STRING
  local subschema = key.schema
  local array = target[id] or {}
  local size = #array
  target[id] = array

  IMGUI.Indent(10)

  local removed = false
  IMGUI.PushStyleVar("ItemSpacing", 8, 4)
  for i,subkey in ipairs(array) do
    IMGUI.PushID(strf("Array.%s##%d", name, i))
    IMGUI.Text(strf("#%d", i))
    INPUTS(id, array[i], subschema)
    if IMGUI.Button("Remove") then
      removed = i
    end
    IMGUI.PopID()
    IMGUI.Spacing()
  end
  if removed then table.remove(array, removed) end

  IMGUI.Separator()
  if IMGUI.Button(strf("Add %s", name)) then
    table.insert(array, {})
  end

  IMGUI.PopStyleVar()
  IMGUI.Unindent(10)

end
