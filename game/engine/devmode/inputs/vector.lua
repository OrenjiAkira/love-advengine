
local IMGUI = require 'imgui'
local FLOAT = require 'game.engine.devmode.inputs.float'
local INTEGER = require 'game.engine.devmode.inputs.integer'

local _EMPTY_STRING = ""
local _DEFAULT_INDEXED = { 'x', 'y', 'z', 'w', 'u', 'v' }

local min, max = math.min, math.max
local function _range(value, range)
  return max(range[1] or value, min(range[2] or value, value))
end

local function _generateVector(size, default, range)
  local vector = {}
  default = default or 0
  for i = 1, size do
    vector[i] = _range(default, range)
  end
  return vector
end

return function(target, key)
  local id = key.id
  local range = key.range or table.empty
  local size = key.size or 2
  local numtype = key.numtype or 'integer'
  local indexed = key.indexed or _DEFAULT_INDEXED
  local default = _generateVector(size, key.default, range)

  target[id] = target[id] or default

  IMGUI.Columns(2, id, false)
  for i = 1, size do
    local name = indexed[i] or _DEFAULT_INDEXED[i] or tostring(i)
    local subkey = {
      id = i, type = numtype, range = range, default = key.default or 0
    }
    IMGUI.PushID(strf("vector#%s:%d", id, i))
    IMGUI.Text(name)
    IMGUI.SameLine()
    if numtype == 'integer' then
      INTEGER(target[id], subkey)
    elseif numtype == 'float' then
      FLOAT(target[id], subkey)
    end
    IMGUI.PopID()
    if i % 2 == 0 then IMGUI.NextColumn() end
    if i % 4 == 0 then IMGUI.Spacing() end
  end
  IMGUI.Columns(1)
  IMGUI.Spacing()
  IMGUI.Spacing()
end
