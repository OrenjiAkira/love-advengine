
local IMGUI = require 'imgui'

local _EMPTY_STRING = ""

local min, max = math.min, math.max
local function _range(value, range)
  return max(range[1] or value, min(range[2] or value, value))
end

return function(target, key)
  local id = key.id
  local range = key.range or {0, 1}
  local default = _range(key.default or 1, range)
  local step = key.step or _DEFAULT_STEP
  local power = key.power or 1

  target[id] = target[id] or default

  local changed, new = IMGUI.SliderFloat(_EMPTY_STRING, target[id],
                                         range[1], range[2], "%.2f", power)
  if changed then
    target[id] = _range(new, range)
  end
end
