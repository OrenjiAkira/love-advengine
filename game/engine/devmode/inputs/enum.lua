
local IMGUI = require 'imgui'

local _EMPTY_STRING = ""
local _NONE = "<no options>"

local function _isFromDB(options)
  return type(options) == 'string'
end

return function(target, key)
  local id = key.id
  local optional = key.optional
  local options = key.options or { _NONE }
  local default = key.default or 1

  if _isFromDB(options) then
    local database, spectype = options:match("(.-)[%./](.+)")
    local db = require(database)
    options = db:listSpecs(spectype)
  end

  if optional then options = { _NONE, unpack(options) } end

  local current = table.find(options, target[id]) or default

  target[id] = target[id] or options[default]
  if target[id] == _NONE then
    target[id] = false
  end

  local changed, new = IMGUI.Combo(_EMPTY_STRING, current,
                                   options, #options, 16)
  if changed then
    target[id] = options[new]
  end

end
