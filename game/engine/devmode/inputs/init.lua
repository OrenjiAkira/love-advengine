
local IMGUI = require 'imgui'
local INPUTS = require 'engine.common.modulepack' 'engine.devmode.inputs'

local _ERR_ID = "No key id given."
local _ERR_TYPE = "No key type given."
local _EMPTY_STRING = ""

return function(group, target, schema)
  IMGUI.BeginGroup()
  IMGUI.Spacing()
  for _,key in ipairs(schema) do
    assert(key.id, _ERR_ID)
    assert(key.type, _ERR_TYPE)
    local id, type = key.id, key.type
    local name = key.name or _EMPTY_STRING
    local help = key.help
    IMGUI.BeginGroup()
    if key.type ~= 'group' then IMGUI.Text(name) end
    IMGUI.PushID(strf("%s:%s#%s: %s", group, type, id, name))
    INPUTS[type](target, key)
    IMGUI.PopID()
    IMGUI.EndGroup()
    if key.type ~= 'group' then IMGUI.Spacing() end
    if help and IMGUI.IsItemHovered() then
      IMGUI.SetTooltip(help)
    end
  end
  return IMGUI.EndGroup()
end
