
local IMGUI = require 'imgui'

-- to avoid cross reference
local INPUTSPATH = 'engine.devmode.inputs'
local INPUTS = package.loaded[INPUTSPATH] or require(INPUTSPATH)

return function(target, key)
  local id = key.id
  local subschema = key.schema
  local name = key.name or key.id
  local no_collapse = key.no_collapse or false
  target[id] = target[id] or {}
  if not no_collapse then
    if IMGUI.CollapsingHeader(name) then
      IMGUI.Indent(10)
      INPUTS(id, target[id], subschema)
      IMGUI.Unindent(10)
      IMGUI.Spacing()
    end
  else
    IMGUI.Text(name)
    IMGUI.Indent(10)
    INPUTS(id, target[id], subschema)
    IMGUI.Unindent(10)
    IMGUI.Spacing()
  end
end
