
return {
  SMOOTH = 8,
  EPSILON = 0.01,
  MARGIN_LEFT = 20,
  MARGIN_TOP = 40,
  PADDING = 16,
  BIG = {
    MIN_W = 480,
    MIN_H = 480,
    MAX_W = 480,
    MAX_H = 480,
  },
  SMALL = {
    MIN_W = 240,
    MIN_H = 80,
    MAX_W = 240,
    MAX_H = 240,
  }
}
