
--[[ EXISTENCE CONDITION ]]--
if not DEVMODE then return false end


--[[ DEPENDENCIES ]]--
local IMGUI    = require 'imgui'
local LAYOUT   = require 'engine.devmode.layout'
local EDITORS  = require 'engine.devmode.editors'
local Visual   = require 'engine.common.visual'
local GAMEDATA = require 'gamedata'
local RESDATA  = require 'resdata'


--[[ CONSTANTS ]]--
local _SIGNALS = { PUSH = 1, POP = 2 }
local _VALID_SIGNAL = { table = true }
local _SPECS = {
  "body", "map", "scene",
  body  = "Bodies",
  map   = "Maps",
  scene = "Scenes",
}
local _RESOURCES = {
  "tileset",
  tileset  = "TileSets",
}

--[[ DEVGUI VISUAL MODULE ]]--
local DEVGUI = Visual:new()

function DEVGUI:init()
  self.focus_stack = {}
  self.focus = 0
end

function DEVGUI:push(editorname, ...)
  assert(editorname, "Invalid editor name.")
  local neweditor = EDITORS[editorname]:new(...)
  self.focus = self.focus + 1
  self.focus_stack[self.focus] = neweditor
end

function DEVGUI:pop()
  self.focus = math.max(0, self.focus - 1)
end

function DEVGUI:set(editorname, ...)
  self.focus = 0
  self:push(editorname, ...)
end

function DEVGUI:updateFocus(index, dt)
  local editor = self.focus_stack[index]
  if not editor then return end
  local size = editor.size or 'BIG'
  local x, y = editor.x, editor.y
  if x then
    IMGUI.SetNextWindowPos(x, y or LAYOUT.MARGIN_TOP)
  else
    IMGUI.SetNextWindowPosCenter()
  end
  IMGUI.SetNextWindowSizeConstraints(LAYOUT[size].MIN_W, LAYOUT[size].MIN_H,
                                     LAYOUT[size].MAX_W, LAYOUT[size].MAX_H
  )
  IMGUI.PushStyleVar("WindowPadding", LAYOUT.PADDING, LAYOUT.PADDING)
  IMGUI.PushStyleVar("ItemSpacing", LAYOUT.PADDING, LAYOUT.PADDING/2)
  local _,open = IMGUI.Begin(editor.name, true,
                             { "NoCollapse", "AlwaysUseWindowPadding",
                             "AlwaysAutoResize" }
  )
  local signal = editor:update(dt)
  IMGUI.End()
  IMGUI.PopStyleVar()
  IMGUI.PopStyleVar()
  if not open then
    self.focus = index
    return self:pop()
  end
  if signal and _VALID_SIGNAL[type(signal)] then
    local action = signal[1]
    if _SIGNALS[action] == _SIGNALS.POP then
      self.focus = index
      return self:pop()
    elseif _SIGNALS[action] == _SIGNALS.PUSH then
      self.focus = index
      return self:push(unpack(signal, 2))
    else
      return signal
    end
  end
end

--[[ IMGUI CALLS AND INPUTS ]]--
function DEVGUI:update(dt)
  local signal = false
  IMGUI.NewFrame()
  if IMGUI.BeginMainMenuBar() then
    if IMGUI.BeginMenu("Project") then
      if IMGUI.MenuItem("Settings") then
        self:set('settings')
      end
      if IMGUI.MenuItem("Reset Game") then
        signal = {'RESET'}
      end
      if IMGUI.MenuItem("Exit DevMode") then
        signal = {'POP'}
      end
      IMGUI.EndMenu()
    end
    if IMGUI.BeginMenu("GameData") then
      for _,spectype in GAMEDATA:eachSpectype() do
        if IMGUI.MenuItem(spectype) then
          self:set('speclist', 'gamedata', spectype)
        end
      end
      IMGUI.Separator()
      if IMGUI.MenuItem("Refresh") then
        GAMEDATA:refresh()
      end
      IMGUI.EndMenu()
    end
    if IMGUI.BeginMenu("ResourceData") then
      for _,spectype in RESDATA:eachSpectype() do
        if IMGUI.MenuItem(spectype) then
          self:set('speclist', 'resdata', spectype)
        end
      end
      IMGUI.Separator()
      if IMGUI.MenuItem("Refresh") then
        RESDATA:refresh()
      end
      IMGUI.EndMenu()
    end
    IMGUI.EndMainMenuBar()
  end

  for i = 1, self.focus do
    local focus_signal = self:updateFocus(i, dt)
    signal = signal or focus_signal
  end

  return signal
end


--[[ RENDER EVERYTHING ]]--
function DEVGUI:draw(g)
  local enter = self.enter
  g.setBackgroundColor(0, 0, 0)
  g.setColor(255, 255, 255, 64*enter)
  g.rectangle('fill', 0, 0, g.getDimensions())
  g.setColor(255, 255, 255, 255*enter)

  for i = 1, self.focus do
    local editor = self.focus_stack[i]
    editor:draw(g)
  end

  IMGUI.Render()
end

return DEVGUI

