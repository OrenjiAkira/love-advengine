
if not DEVMODE then
  return {
    init = function()
      return error("Cannot start devtools when not in DEVMODE.")
    end
  }
end

--[[ DEPENDENCIES ]]--
local INPUT = require 'engine.input'
local DEVGUI = require 'engine.devmode.view'
local IMGUI = require 'imgui'


--[[ CONSTANTS ]]--
local _IMGUI_BINDINGS = {
  keypressed = function (key)
    IMGUI.KeyPressed(key)
  end,
  textinput = function (t)
    IMGUI.TextInput(t)
  end,
  keyreleased = function (key)
    IMGUI.KeyReleased(key)
  end,
  mousemoved = function (x, y)
    IMGUI.MouseMoved(x, y)
  end,
  mousepressed = function (x, y, button)
    IMGUI.MousePressed(button)
  end,
  mousereleased = function (x, y, button)
    IMGUI.MouseReleased(button)
  end,
  wheelmoved = function (x, y)
    IMGUI.WheelMoved(y)
  end,
}
local _OPEN_CLOSE_TIME = .4


--[[ LOCALS ]]--
local _oldkeybinds = {}


--[[ STATE METHODS ]]--
local state = {}
local _gui

function state.init()
end

function state.enter(from, ...)
  love.mouse.setVisible(true)
  for bind, imgui_event in pairs(_IMGUI_BINDINGS) do
    local old = love[bind] or __NOTHING__
    _oldkeybinds[bind] = old
    love[bind] = function(...)
      old(...)
      imgui_event(...)
    end
  end
  _gui = DEVGUI:new()
  _gui:setLayer("GUI")
  _gui:open(_OPEN_CLOSE_TIME)
  print("> ENTER DEVMODE!")
end

function state.leave()
  love.mouse.setVisible(false)
  for bind in pairs(_IMGUI_BINDINGS) do
    local old = _oldkeybinds[bind]
    love[bind] = old
  end
  _gui:close(_OPEN_CLOSE_TIME)
  print("> QUIT DEVMODE!")
end

function state.update(dt)
  if not IMGUI.GetWantCaptureKeyboard() then
    if INPUT.wasActionReleased('DEBUG_QUIT') then
      return {'QUIT'}
    elseif INPUT.wasActionPressed('DEBUG_DEVMODE') then
      return {'POP'}
    elseif INPUT.wasActionPressed('DEBUG_CANCEL') then
      print("DEBUG CANCEL!")
      _gui:pop()
    end
  end
  return _gui:update(dt)
end

return state
