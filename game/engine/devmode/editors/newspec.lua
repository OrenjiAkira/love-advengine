
--[[ DEPENDENCIES ]]--
local IMGUI = require 'imgui'

local INPUT     = require 'engine.input'
local TIMER     = require 'engine.common.timer'
local LAYOUT    = require 'engine.devmode.layout'
local Prototype = require 'engine.common.prototype'


--[[ CONSTANTS ]]--
local _EMPTY_STRING = ""
local _RESOURCE = "Resource" 
local _SPEC = "Spec" 


--[[ EDITOR METHODS ]]--
local Editor = Prototype:new()

function Editor:init(database, spectype)
  self.name = strf("New %s", spectype)
  self.size = "SMALL"
  self.database = database
  self.spectype = spectype
  self.new_name = _EMPTY_STRING

  local w = LAYOUT[self.size].MAX_W
  self.x = -w
  TIMER.tween(0.25, self, {x = w + 2*LAYOUT.MARGIN_LEFT}, "out-back")
end

function Editor:update(dt)
  local action

  -- new name input
  local changed, new = IMGUI.InputText(_EMPTY_STRING, self.new_name, 16)
  if changed then
    self.new_name = new
  end

  -- validate and confirm
  local valid_name = #self.new_name > 0
  local confirm = INPUT.wasActionPressed('DEBUG_CONFIRM')
  confirm = IMGUI.Button("Add") or confirm
  if confirm and valid_name then
    local db = require(self.database)
    db:createSpec(self.spectype, self.new_name)
    printf("Added new %s#%s: %s", self.database, self.spectype, self.new_name)
    action = {'POP'}
  end

  return action
end

return Editor
