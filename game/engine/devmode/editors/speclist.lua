
--[[ DEPENDENCIES ]]--
local IMGUI = require 'imgui'

local TIMER     = require 'engine.common.timer'
local LAYOUT    = require 'engine.devmode.layout'
local Prototype = require 'engine.common.prototype'

--[[ CONSTANTS ]]--
local _EMPTY_STRING = ""


--[[ EDITOR METHODS ]]--
local Editor = Prototype:new()

function Editor:init(database, spectype)
  self.name = strf("DB: %s", spectype)
  self.database = database
  self.spectype = spectype
  self.size = "SMALL"
  self.selected = 0


  self.x = -LAYOUT[self.size].MAX_W
  TIMER.tween(0.25, self, {x = LAYOUT.MARGIN_LEFT}, "out-back")
end

function Editor:update(dt)
  local action
  local selected = self.selected
  local database = self.database
  local spectype = self.spectype

  local db = require(database)
  local items = db:listSpecs(spectype)

  IMGUI.Spacing()
  IMGUI.TextWrapped(strf("Pick a %s", spectype))
  IMGUI.Spacing()

  if IMGUI.Button(strf("Add %s", spectype)) then
    action = {'PUSH', 'newspec', database, spectype}
  end

  local changed, selected = IMGUI.ListBox(_EMPTY_STRING, selected,
                                          items, #items, 5)

  if changed then
    local item = items[selected]
    local new_editor = strf('spec.%s', spectype)
    print(spectype, new_editor, item)
    action = {'PUSH', new_editor, item}
  end
  IMGUI.Spacing()

  return action
end

return Editor

