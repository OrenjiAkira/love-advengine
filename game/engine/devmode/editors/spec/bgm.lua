
--[[ DEPENDENCIES ]]--
local IMGUI = require 'imgui'

local Prototype = require 'engine.common.prototype'
local LAYOUT    = require 'engine.devmode.layout'
local INPUTS    = require 'engine.devmode.inputs'
local SCHEMA    = require 'resdata.schema.bgm'
local RESDATA   = require 'resdata'
local RESLOADER = require 'resources'

local min, max = math.min, math.max

--[[ CONSTANTS ]]--
local _WIDTH, _HEIGHT


--[[ EDITOR METHODS ]]--
local Editor = Prototype:new()

function Editor:init(specname)
  _WIDTH, _HEIGHT = love.graphics.getDimensions()
  self.name = strf("Edit BGM: %s", specname)
  self.size = "BIG"
  self.x = _WIDTH - LAYOUT.MARGIN_LEFT - LAYOUT[self.size].MAX_W

  self.target = RESDATA:loadSpec('bgm', specname)
  self.loader = false
  self.specname = specname
end

function Editor:refresh()
  if self.loader then
    self.loader:stop()
    self.loader:refresh()
  end
end

function Editor:update(dt)
  local action

  if IMGUI.Button("Refresh##top") then
    self:refresh()
  end
  IMGUI.SameLine()
  if IMGUI.Button("Save##top") then
    self:refresh()
    RESDATA:save('bgm', self.specname)
  end

  IMGUI.Separator()

  INPUTS("BGM Info", self.target, SCHEMA)

  if not self.loader and self.target.filename then
    self.loader = RESLOADER.load('bgm', self.specname)
  end

  if self.loader and IMGUI.Button("Play/Pause") then
    if self.loader:isPlaying() then
      self.loader:pause()
    else
      self.loader:play()
    end
  end

  IMGUI.Separator()

  if IMGUI.Button("Refresh##bottom") then
    self:refresh()
  end
  IMGUI.SameLine()
  if IMGUI.Button("Save##bottom") then
    self:refresh()
    RESDATA:save('bgm', self.specname)
  end

  return action
end

return Editor
