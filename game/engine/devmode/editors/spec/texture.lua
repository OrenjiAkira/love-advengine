
--[[ DEPENDENCIES ]]--
local IMGUI = require 'imgui'

local Prototype = require 'engine.common.prototype'
local LAYOUT    = require 'engine.devmode.layout'
local INPUTS    = require 'engine.devmode.inputs'
local SCHEMA    = require 'resdata.schema.texture'
local RESDATA   = require 'resdata'
local RESLOADER = require 'resources'

local min, max = math.min, math.max

--[[ CONSTANTS ]]--
local _WIDTH, _HEIGHT


--[[ EDITOR METHODS ]]--
local Editor = Prototype:new()

function Editor:init(specname)
  _WIDTH, _HEIGHT = love.graphics.getDimensions()
  self.name = strf("Edit texture: %s", specname)
  self.size = "BIG"
  self.x = _WIDTH - LAYOUT.MARGIN_LEFT - LAYOUT[self.size].MAX_W

  self.target = RESDATA:loadSpec('texture', specname)
  self.loader = false
  self.specname = specname
end

function Editor:refresh()
  if self.loader then self.loader:refresh() end
end

function Editor:update(dt)
  local action

  if IMGUI.Button("Refresh##top") then
    self:refresh()
  end
  IMGUI.SameLine()
  if IMGUI.Button("Save##top") then
    self:refresh()
    RESDATA:save('texture', self.specname)
  end

  IMGUI.Separator()

  INPUTS("Texture Info", self.target, SCHEMA)

  if not self.loader and self.target.filename then
    self.loader = RESLOADER.load('texture', self.specname)
  end
  if self.loader then self:drawTexture() end

  IMGUI.Separator()

  if IMGUI.Button("Refresh##bottom") then
    self:refresh()
  end
  IMGUI.SameLine()
  if IMGUI.Button("Save##bottom") then
    self:refresh()
    RESDATA:save('texture', self.specname)
  end

  return action
end

function Editor:drawTexture()
  local tex = self.loader:getTexture()
  local iw, ih = tex:getDimensions()
  local max_w = LAYOUT[self.size].MAX_W - 32
  local max_h = LAYOUT[self.size].MAX_H / 2
  local proportions = iw/ih
  local w, h = min(iw, max_w), min(ih, max_h)

  if iw > ih then
    h = w / proportions
  elseif iw < ih then
    w = h * proportions
  end

  IMGUI.Image(tex, w, h)
end

return Editor
