
--[[ DEPENDENCIES ]]--
local IMGUI = require 'imgui'

local Prototype = require 'engine.common.prototype'
local LAYOUT    = require 'engine.devmode.layout'
local INPUTS    = require 'engine.devmode.inputs'
local SCHEMA    = require 'resdata.schema.sprite'
local RESDATA   = require 'resdata'
local RESLOADER = require 'resources'

local min = math.min

--[[ CONSTANTS ]]--
local _WIDTH, _HEIGHT


--[[ EDITOR METHODS ]]--
local Editor = Prototype:new()

function Editor:init(specname)
  _WIDTH, _HEIGHT = love.graphics.getDimensions()
  self.name = strf("Edit sprite: %s", specname)
  self.size = "BIG"
  self.x = _WIDTH - LAYOUT.MARGIN_LEFT - LAYOUT[self.size].MAX_W

  self.target = RESDATA:loadSpec('sprite', specname)
  self.loader = false
  self.specname = specname
end

function Editor:refresh()
  if self.loader then self.loader:refresh() end
end

function Editor:update(dt)
  local action
  local target = self.target

  if IMGUI.Button("Refresh##top") then
    self:refresh()
  end
  IMGUI.SameLine()
  if IMGUI.Button("Save##top") then
    self:refresh()
    RESDATA:save('sprite', self.specname)
  end

  IMGUI.Separator()

  if self.loader then self:previewAnimation(dt) end
  INPUTS("Sprite Info", target, SCHEMA)
  local rows, cols = unpack(target.divisions)
  local qcount = rows*cols
  for _,frame in ipairs(target.frames) do
    if frame.qidx then
      frame.qidx = min(frame.qidx, qcount)
    end
  end

  if not self.loader and target.filename then
    self.loader = RESLOADER.load('sprite', self.specname)
  end

  IMGUI.Separator()

  if IMGUI.Button("Refresh##bottom") then
    self:refresh()
  end
  IMGUI.SameLine()
  if IMGUI.Button("Save##bottom") then
    self:refresh()
    RESDATA:save('sprite', self.specname)
  end

  return action
end

function Editor:previewAnimation(dt)
  local loader = self.loader
  local quad = loader:getCurrentQuad()
  if not quad then return end
  local tex = loader:getTexture()
  local iw, ih = tex:getDimensions()
  local x, y, qw, qh = quad:getViewport()
  local u1, v1 = x / iw, y / ih
  local u2, v2 = (x+qw) / iw, (y+qh) / ih
  loader:update(dt)
  IMGUI.Image(tex, qw, qh, u1, v1, u2, v2)
end

return Editor
