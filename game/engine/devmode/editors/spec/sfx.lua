
--[[ DEPENDENCIES ]]--
local IMGUI = require 'imgui'

local Prototype = require 'engine.common.prototype'
local LAYOUT    = require 'engine.devmode.layout'
local INPUTS    = require 'engine.devmode.inputs'
local SCHEMA    = require 'resdata.schema.sfx'
local RESDATA   = require 'resdata'
local RESLOADER = require 'resources'

local min, max = math.min, math.max

--[[ CONSTANTS ]]--
local _WIDTH, _HEIGHT


--[[ EDITOR METHODS ]]--
local Editor = Prototype:new()

function Editor:init(specname)
  _WIDTH, _HEIGHT = love.graphics.getDimensions()
  self.name = strf("Edit SFX: %s", specname)
  self.size = "BIG"
  self.x = _WIDTH - LAYOUT.MARGIN_LEFT - LAYOUT[self.size].MAX_W

  self.target = RESDATA:loadSpec('sfx', specname)
  self.loader = false
  self.specname = specname
end

function Editor:refresh()
  if self.loader then
    self.loader:stop()
    self.loader:refresh()
  end
end

function Editor:update(dt)
  local action

  if IMGUI.Button("Refresh##top") then
    self:refresh()
  end
  IMGUI.SameLine()
  if IMGUI.Button("Save##top") then
    self:refresh()
    RESDATA:save('sfx', self.specname)
  end

  IMGUI.Separator()

  INPUTS("SFX Info", self.target, SCHEMA)

  if not self.loader and self.target.filename then
    self.loader = RESLOADER.load('sfx', self.specname)
  end

  if self.loader and IMGUI.Button("Play") then
    self.loader:play()
  end

  IMGUI.Separator()

  if IMGUI.Button("Refresh##bottom") then
    self:refresh()
  end
  IMGUI.SameLine()
  if IMGUI.Button("Save##bottom") then
    self:refresh()
    RESDATA:save('sfx', self.specname)
  end

  return action
end

return Editor
