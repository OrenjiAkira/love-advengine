
--[[ DEPENDENCIES ]]--
local IMGUI = require 'imgui'

local Prototype = require 'engine.common.prototype'
local LAYOUT    = require 'engine.devmode.layout'
local INPUTS    = require 'engine.devmode.inputs'
local SCHEMA    = require 'resdata.schema.tileset'
local RESDATA   = require 'resdata'
local RESLOADER = require 'resources'

local floor = math.floor

--[[ CONSTANTS ]]--
local _COLLISIONS = {
  '.', '#', 'y', 'u', 'n', 'b',
  ['.'] = "...\n...\n...",
  ['#'] = "###\n###\n###",
  ['y'] = "..#\n.##\n###",
  ['u'] = "#..\n##.\n###",
  ['n'] = "###\n##.\n#..",
  ['b'] = "###\n.##\n..#",
}
local _WIDTH
local _HEIGHT


--[[ EDITOR METHODS ]]--
local Editor = Prototype:new()

function Editor:init(specname)
  _WIDTH, _HEIGHT = love.graphics.getDimensions()
  self.size = "BIG"
  self.name = strf("Edit tileset: %s", specname)
  self.target = RESDATA:loadSpec('tileset', specname)
  self.loader = false
  self.specname = specname
  self.x = _WIDTH - LAYOUT.MARGIN_LEFT - LAYOUT[self.size].MAX_W
  self:refresh()
end

function Editor:refresh()
  local rows, cols = unpack(self.target.divisions or {1, 1})
  self.tilecount = rows*cols
  self.tile = 1
  if self.loader then self.loader:refresh() end
end

function Editor:update(dt)
  local action

  if IMGUI.Button("Refresh##top") then
    self:refresh()
  end
  IMGUI.SameLine()
  if IMGUI.Button("Save##top") then
    RESDATA:save('tileset', self.specname)
    self:refresh()
  end

  IMGUI.Separator()

  INPUTS("TileSet Info", self.target, SCHEMA)

  if not self.loader and self.target.filename then
    self.loader = RESLOADER.load('tileset', self.specname)
  end
  if self.loader then self:drawCurrentTile() end

  IMGUI.Separator()

  if IMGUI.Button("Refresh##bottom") then
    self:refresh()
  end
  IMGUI.SameLine()
  if IMGUI.Button("Save##bottom") then
    RESDATA:save('tileset', self.specname)
    self:refresh()
  end

  return action
end

function Editor:drawCurrentTile()
  local tex = self.loader:getTexture()
  local qw, qh = self.loader:getQuadDimensions()
  local u, v = self.loader:getQuadUV(self.tile)
  local iw, ih = tex:getDimensions()
  local tile = self.tile
  local tilecount = self.tilecount
  local collisions = self.target.collisions or {}
  self.target.collisions = collisions

  for idx = 1, tilecount do
    collisions[idx] = collisions[idx] or _COLLISIONS[1]
  end
  local coltype = collisions[tile]
  local colidx = table.find(_COLLISIONS, coltype)

  IMGUI.Text("Tile Editor")
  IMGUI.Text(strf("Tile no. #%02d/%02d", tile, tilecount))
  IMGUI.Image(tex, qw, qh, u/iw, v/ih, (u+qw)/iw, (v+qh)/ih)

  IMGUI.SameLine()
  IMGUI.Text(_COLLISIONS[coltype])

  if IMGUI.Button("Change Collision") then
    local newidx = colidx % #_COLLISIONS + 1
    collisions[tile] = _COLLISIONS[newidx]
  end

  if IMGUI.Button("Prev Tile")  then
    self.tile = (tile + tilecount - 2) % tilecount + 1
  end
  IMGUI.SameLine()
  if IMGUI.Button("Next Tile") then
    self.tile = tile % tilecount + 1
  end

end

return Editor

