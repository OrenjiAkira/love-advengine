
--[[ DEPENDENCIES ]]--
local IMGUI = require 'imgui'

local Prototype = require 'engine.common.prototype'
local LAYOUT    = require 'engine.devmode.layout'
local INPUTS    = require 'engine.devmode.inputs'
local SCHEMA    = require 'gamedata.schema.body'
local GAMEDATA  = require 'gamedata'

--[[ CONSTANTS ]]--
local _WIDTH, _HEIGHT


--[[ EDITOR METHODS ]]--
local Editor = Prototype:new()

function Editor:init(specname)
  _WIDTH, _HEIGHT = love.graphics.getDimensions()
  self.name = strf("Edit body: %s", specname)
  self.size = "BIG"
  self.x = _WIDTH - LAYOUT.MARGIN_LEFT - LAYOUT[self.size].MAX_W

  self.target = GAMEDATA:loadSpec('body', specname)
  self.loader = false
  self.specname = specname
end

function Editor:update(dt)
  local action

  if IMGUI.Button("Save##top") then
    GAMEDATA:save('body', self.specname)
  end

  IMGUI.Separator()

  INPUTS("Body Info", self.target, SCHEMA)

  IMGUI.Separator()

  if IMGUI.Button("Save##bottom") then
    GAMEDATA:save('body', self.specname)
  end

  return action
end

return Editor

