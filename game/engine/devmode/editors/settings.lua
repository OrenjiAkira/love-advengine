
local SETTINGS = SETTINGS

local IMGUI = require 'imgui'

local CONSTS    = require 'engine.consts'
local ENCODER   = require 'engine.misc.encoder'
local LAYOUT    = require 'engine.devmode.layout'
local INPUTS    = require 'engine.devmode.inputs'
local Prototype = require 'engine.common.prototype'
local SCHEMA    = require 'meta.schema'

local Editor = Prototype:new()

function Editor:init()
  self.name = "Project Settings"
  self.size = "BIG"
end

function Editor:update(dt)
  local action
  IMGUI.Spacing()
  IMGUI.TextWrapped("Note: The game must be reset before "
                    .."some changes take effect.")
  IMGUI.Spacing()
  if IMGUI.Button("Save") then
    ENCODER.writeData(CONSTS.SETTINGS_FILEPATH, SETTINGS, true)
  end
  IMGUI.SameLine()
  if IMGUI.Button("Reset Game") then
    action = {'RESET'}
  end
  IMGUI.NewLine()
  INPUTS("settings", SETTINGS, SCHEMA)
  IMGUI.Spacing()
  IMGUI.Spacing()
  return action
end

return Editor
