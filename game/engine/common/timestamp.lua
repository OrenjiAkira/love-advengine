
local TIME = {}

local floor = math.floor

function TIME.toHuman(raw)
  return strf("%02d:%02d:%02d",
              floor(raw/60^2),
              floor(raw/60),
              raw % 60)
end

function TIME.toRaw(human)
  local h, m, s = human:match("(%d%d):(%d%d):(%d%d)")
  return tonumber(h) + tonumber(m) + tonumber(s) or 0
end

return TIME

