
--[[ DEPENDENCIES ]]--
local GAMEDATA = require 'gamedata'
local Prototype = require 'engine.common.prototype'


--[[ GAME ELEMENT CLASS ]]--
local GameElement = Prototype:new()


--[[ CONSTANTS ]]--
local _NULL = 0
local _VALID_ID_TYPE = { string = true, number = true }
local _ERR_ID_TYPE = "Invalid ID type. Must be number or string."
local _ERR_ID_VALUE = "Invalid ID value (must not be ".._NULL..")"
local _ERR_ID_USED = "ID already used"


--[[ LOCAL VARIABLES ]]--
local _all_elements = {}
local _dirty = false


--[[ INIT METHODS ]]--
function GameElement:init()
  self.id = false
end

function GameElement:construct(spectype, specname)
  self.spectype = spectype
  self.specname = specname
end

function GameElement:load(data)
end

function GameElement:save()
  return {}
end


--[[ ID RELATED METHODS ]]--

-- static method for finding any element with valid id
function GameElement.find(id)
  return _all_elements[id] 
end

-- static method for cleaning all destroyed elements
function GameElement.clean()
  if _dirty then
    for id, e in pairs(_all_elements) do
      if id == _NULL then _all_elements[id] = nil end
    end
    _dirty = false
  end
end

-- static method to clear all elements
function GameElement.clearAll()
  for id, e in pairs(_all_elements) do
    _all_elements[id] = nil
  end
end

function GameElement:getID()
  return self.id
end

function GameElement:setID(id)
  -- this also initializes the element for searching
  assert(_VALID_ID_TYPE[type(id)], _ERR_ID_TYPE)
  assert(id ~= _NULL, _ERR_ID_VALUE)
  assert(not _all_elements[id] or _all_elements[id] == self, _ERR_ID_USED)
  -- everything ok, carry on
  local old = self.id
  _all_elements[old or 0] = nil
  _all_elements[id] = self
  self.id = id
end

function GameElement:destroy()
  self.id = _NULL
  _dirty = true
end


--[[ SPEC & DB RELATED METHODS ]]--
function GameElement:getSpecType()
  return self.spectype
end

function GameElement:getSpecName()
  return self.specname
end

function GameElement:getFromSpec(field)
  local spec = GAMEDATA.loadSpec(self.spectype, self.specname)
  assert(spec, strf("Spec not found: %s/%s", self.spectype, self.specname))
  return spec[field]
end


return GameElement

