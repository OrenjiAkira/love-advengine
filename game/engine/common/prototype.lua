
local Prototype = {}


--[[ RECURSIVE CONSTRUCTOR ]]--
local function _init(obj, super, params)
  if not super then return end
  _init(obj, super:super(), params)
  return (super.init and super.init or __NOTHING__)(obj, unpack(params))
end


--[[ INIT METHODS ]]--
function Prototype:new(...)
  local params = table.pack(...)
  local obj = setmetatable({}, self)
  obj.__index = obj
  _init(obj, self, params)
  return obj
end

function Prototype:super()
  return getmetatable(self)
end

function Prototype:init(...)
end


--[[ PLACEHOLDER METHODS ]]--
function Prototype:construct()
  -- use for constructing child classes from parent's constructor
end

function Prototype:update(dt)
  -- use for updating logic
end

function Prototype:draw(g)
  -- use for rendering
end


--[[ DEFAULT METAMETHODS ]]--
function Prototype:__index(k)
  return getmetatable(self)[k]
end


return Prototype
