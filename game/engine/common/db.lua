
--[[ DEPENDENCIES ]]--
local Prototype = require 'engine.common.prototype'
local ENCODER   = require 'engine.misc.encoder'
local FS = love.filesystem

--[[ CONSTANTS ]]--
local _ERR_INVALID_BASEDIR = [=[
First arg to new DB must be base directory path.]=]
local _ERR_INVALID_TYPELIST = [=[
Second arg to new DB must be array of strings]=]
local _DELETE = -65536

--[[ COMMON LOCAL FUNCTIONS ]]--
local function _newCache(basedir, spectypes)
  local cache = {}

  for _,spectype in ipairs(spectypes) do
    local specgroup = setmetatable({}, {spectype = spectype})
    local relpath = strf("%s/%s", basedir, spectype)
    local filelist = FS.getDirectoryItems(relpath)
    for n,filename in ipairs(filelist) do
      local specname = filename:match("^(.+)[.]json")
      local filepath = strf("%s/%s", relpath, specname)
      local spec = ENCODER.loadData(filepath)
      specgroup[specname] = spec
    end
    cache[spectype] = specgroup
  end

  return cache
end

local function _saveData(data, basedir, spectype, specname)
  local relpath = strf("%s/%s/%s", basedir, spectype, specname)
  ENCODER.writeData(relpath, data, true)
end

local function _deleteData(basedir, spectype, specname)
  local relpath = strf("%s/%s/%s", basedir, spectype, specname)
  ENCODER.deleteData(relpath, true)
end

--[[ DATABASE MODULE ]]--
local DB = Prototype:new()

function DB:init(basedir, spectypes)
  assert(isstring(basedir) and FS.isDirectory(basedir), _ERR_INVALID_BASEDIR)
  assert(istable(spectypes) and isstring(spectypes[1]), _ERR_INVALID_TYPELIST)
  self.basedir = basedir
  self.spectypes = spectypes
  self.cache = _newCache(basedir, spectypes)
end

function DB:refresh()
  local cache = self.cache
  local basedir = self.basedir
  for _,spectype in ipairs(self.spectypes) do
    for specname, data in pairs(cache[spectype]) do
      if data == _DELETE then
        _deleteData(basedir, spectype, specname)
      end
    end
  end
  self:init(self.basedir, self.spectypes)
end

function DB:save(spectype, specname)
  local cache = self.cache
  local basedir = self.basedir
  if spectype and specname then
    -- save a single spec
    local data = cache[spectype][specname]
    _saveData(data, basedir, spectype, specname)
  elseif spectype then
    -- save a group of specs
    for specname in pairs(cache[spectype]) do
      local data = cache[spectype][specname]
      _saveData(data, basedir, spectype, specname)
    end
  else
    -- save all of the database
    for _,spectype in ipairs(self.spectypes) do
      for specname in pairs(cache[spectype]) do
        local data = cache[spectype][specname]
        _saveData(cache, basedir, spectype, specname)
      end
    end
  end
end

function DB:delete(spectype, specname)
  self.cache[spectype][specname] = _DELETE
end

function DB:rename(spectype, from, to)
  local cache = self.cache
  local data = cache[spectype][from]
  cache[spectype][to] = data
  self:delete(spectype, from)
end

function DB:loadSpec(spectype, specname)
  return (self.cache[spectype] or table.empty)[specname]
end

function DB:createSpec(spectype, specname)
  self.cache[spectype][specname] = {}
end

function DB:schemaFor(spectype)
  local basedir = self.basedir
  local relpath = strf("%s.schema.%s", basedir, spectype)
  return ipairs(require(relpath))
end

function DB:eachSpectype()
  return ipairs(self.spectypes)
end

function DB:listSpecs(spectype)
  local specgroup = self.cache[spectype]
  local specs = {}
  for specname, data in pairs(specgroup) do
    if data ~= _DELETE then
      table.insert(specs, specname)
    end
  end
  return specs
end

return DB

