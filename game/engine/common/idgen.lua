
local _BASE = 1000

return function (next_id)
  next_id = next_id or 0
  return {
    generate = function()
      local id = strf("#%d", _BASE + next_id)
      next_id = next_id + 1
      return id
    end,
    getNextId = function()
      return next_id
    end
  }
end

