
--[[ DEPENDENCIES ]]--
local SCREEN = require 'engine.screen'
local TIMER = require 'engine.common.timer'
local Prototype = require 'engine.common.prototype'

--[[ CONSTANTS ]]--
local _ENTER_TWEEN = 'enter_tween'


--[[ VISUAL CLASS ]]--
local Visual = Prototype:new()

--[[ INIT METHODS ]]--
function Visual:init()
  self.timers = {}
  self.enter = 0
end


--[[ LAYER & VISIBILITY METHODS ]]--
function Visual:setLayer(layer)
  SCREEN.add(self, layer)
end

function Visual:destroy()
  self.dead = true
  SCREEN.setDirty()
end

function Visual:isDead()
  return not not self.dead
end

function Visual:setInvisible(invisible)
  self.invisible = not not invisible
end

function Visual:isInvisible()
  return not not self.invisible
end


--[[ TIMER METHODS ]]--
function Visual:addTimer(name, ...)
  local params = table.pack(...)
  self.timers[name] = TIMER[params[1]](unpack(params, 2))
end

function Visual:removeTimer(name)
  local handle = self.timers[name]
  if not handle then return end
  self.timers[name] = nil
  TIMER.cancel(handle)
end


--[[ COMMON ]]--
function Visual:open(time)
  self:removeTimer(_ENTER_TWEEN)
  self:addTimer(_ENTER_TWEEN, 'tween', time,
                self, { enter = 1 }, 'out-quad'
  )
end

function Visual:close(time)
  self:removeTimer(_ENTER_TWEEN)
  self:addTimer(_ENTER_TWEEN, 'tween', time,
                self, { enter = 0 }, 'out-quad',
                function() self:destroy() end
  )
end

return Visual
