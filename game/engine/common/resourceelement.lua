
local RESDATA = require 'resdata'
local Prototype = require 'engine.common.prototype'

local ResourceElement = Prototype:new()

function ResourceElement:init(specname)
  self.specname = specname
end

function ResourceElement:construct(spectype)
  self.spectype = spectype
end

function ResourceElement:refresh()
  self:init(self.specname)
end

function ResourceElement:getFromSpec(field)
  local spec = RESDATA:loadSpec(self.spectype, self.specname)
  assert(spec, strf("No such entry in data base: %s/%s",
                    self.spectype, self.specname)
  )
  return spec[field]
end

return ResourceElement

