
local STATE = {}

local _stack
local _level
local _next
local _actions = {}

local function _current()
  if not _stack or not _level then
    return false, strf("STATE module is not initialised!\n%s",
                       "Try calling STATE.start first!")
  end
  if not _stack[_level] then
    return false, strf("STATE module is set to an invalid index [%s]!",
                       _level)
  end
  return _stack[_level]
end

local function _init(state)
  return (state.init ~= false
         and (function()
           local init = state.init
           state.init = false
           return init
         end)() or __NOTHING__)()
end

local function _change(from, to, offset, ...)
  (offset <= 0 and from.leave or __NOTHING__)()
  _level = _level + offset
  _stack[_level] = to or _stack[_level]
  return ((to and to.init and _init) or __NOTHING__)(to)
end

function _actions.PUSH(to, ...)
  local from = assert(_current())
  _change(from, to, 1, ...)
  return (to.enter and to.enter or __NOTHING__)(from, ...)
end

function _actions.POP(...)
  local from = assert(_current())
  _change(from, false, -1, ...)
  local to = assert(_current())
  return (to.resume and to.resume or __NOTHING__)(from, ...)
end

function _actions.SWITCH(to, ...)
  local from = assert(_current())
  _change(from, to, 0, ...)
  return (to.enter and to.enter or __NOTHING__)(from, ...)
end

function _actions.RESET()
  local from = assert(_current());
  (from.leave or __NOTHING__)()
  print("RESET REQUESTED")
  love.reset()
end

function _actions.QUIT()
  local from = assert(_current());
  (from.leave or __NOTHING__)()
  print("QUIT REQUESTED")
  love.event.quit()
end

function STATE.start(first, ...)
  _level = 1
  _stack = {}
  _stack[_level] = first
  ;(first.enter and first.enter or __NOTHING__)(false, ...)
end

function STATE.check()
  if not _next then return end
  _actions[_next[1]](unpack(_next, 2))
  _next = nil
end

function STATE.update(dt)
  local current = assert(_current())
  local signal = (current and current.update or __NOTHING__)(dt)
  if signal and _actions[signal[1]] then _next = signal end
end

return STATE

