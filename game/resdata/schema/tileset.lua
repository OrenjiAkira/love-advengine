
local ASSETS = require 'assets'
local imgfiles = ASSETS.listFiles('img')

return {
  { id = 'filename', name = "Texture Filename", type = 'enum', options = imgfiles,
    help = "TileSet's base texture" },
  { id = 'offset', name = "Offset", type = 'vector', numtype = 'integer',
    range = {0}, indexed = {'ox', 'oy'},
    help = "Tile rendering offset"
  },
  { id = 'divisions', name = "Quads", type = 'vector', numtype = 'integer',
    range = {1}, indexed = {'rows', 'cols'},
    help = "The amount of rows and columns in which the"
           .."texture should be divided."
  },
  { id = 'has_safety_margin', name = "One-pixel Safety Margin",
    type = 'boolean',
    help = "Sometimes tiles 'bleed', and a one-pixel margin is your answer.\n"
           .."Needs each tile to be 2 pixels wider in each dimension.\n"
           .."(One pixel to each of the four directions.)", },
}
