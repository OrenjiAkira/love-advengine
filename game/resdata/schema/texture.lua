
local ASSETS = require 'assets'
local imgfiles = ASSETS.listFiles('img')

return {
  { id = 'filename', name = "Texture Filename", type = 'enum',
    options = imgfiles, help = "Texture filename."},
}

