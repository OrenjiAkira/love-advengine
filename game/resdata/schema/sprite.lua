
local ASSETS = require 'assets'
local imgfiles = ASSETS.listFiles('img')

return {
  { id = 'filename', name = "Texture Filename", type = 'enum',
    options = imgfiles, help = "Texture filename."},
  { id = 'offset', name = "Offset", type = 'vector', numtype = 'integer',
    range = {0}, indexed = {'ox', 'oy'},
    help = "Sprite rendering offset"
  },
  { id = 'divisions', name = "Quads", type = 'vector', numtype = 'integer',
    range = {1}, indexed = {'rows', 'cols'},
    help = "The amount of rows and columns in which the"
           .."texture should be divided."
  },
  { id = 'loop', name = "Loop", type = 'boolean',
    help = "When checked, animation will loop indefinitely."
           .."When unchecked, it will freeze at the last frame."
  },
  { id = 'frames', name = "Animation Frames", type = 'array',
    schema = {
      { id = 'qidx', name = "Quad Index", type = 'integer', range = {1},
        help = "Which quad division of the sprite this frame corresponds to."
      },
      { id = 'time', name = "Miliseconds", type = 'integer', range = {1,2000},
        help = "How many miliseconds are spent in this frame."
      },
    },
  }
}
