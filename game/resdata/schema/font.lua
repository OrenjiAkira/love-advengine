
local ASSETS = require 'assets'
local fontfiles = ASSETS.listFiles('font')

return {
  { id = 'filename', name = "Font Filename", type = 'enum',
    options = fontfiles, help = "Font filename.",
  },
  { id = 'size', name = "Size", type = 'integer', range = {4},
    default = 12, help = "Font size in pixels. Not actual visual font-height.",
  },
  { id = 'lh', name = "Line Height", type = 'float', range = {0.5, 3},
    default = 1.0, help = "Line height. Relative to font's size.",
  }
}

