# paths
GAME=game
EXTERNALS=externals
RELEASES=release
RELEASE_BUNDLE=$(RELEASES)/game.love
LIBS=$(GAME)/libs

# lib imgui
LIB_IMGUI=imgui.so
REPO_IMGUI=$(EXTERNALS)/love-imgui
REMOTE_IMGUI=https://github.com/slages/love-imgui.git
SO_IMGUI=$(REPO_IMGUI)/build/imgui.so

# lib dkjson
LIB_DKJSON=$(LIBS)/dkjson.lua

# lib hump
LIB_HUMP=$(LIBS)/hump
REPO_HUMP=$(EXTERNALS)/hump
REMOTE_HUMP=https://github.com/vrld/hump.git

# lib cpml
LIB_CPML=$(LIBS)/cpml
REPO_CPML=$(EXTERNALS)/cpml
REMOTE_CPML=https://github.com/excessive/cpml.git

# lib acl2d
LIB_ACL2D=$(LIBS)/acl2d
REPO_ACL2D=$(EXTERNALS)/acl2d
REMOTE_ACL2D=https://github.com/orenjiakira/acl2d.git

# dependencies
DEPENDENCIES=$(LIB_IMGUI) $(LIB_DKJSON) $(LIB_HUMP) $(LIB_CPML) $(LIB_ACL2D)



######################
### commands
######################

.PHONY: all clean purge deploy

all: $(EXTERNALS) $(DEPENDENCIES)
	love game

deploy: $(RELEASE_BUNDLE)
	@echo "Your release package should be in: $(RELEASE_BUNDLE)"

clean:
	@rm -rfv $(RELEASE) $(DEPENDENCIES)

purge: clean
	@rm -rfv $(EXTERNALS)/*



######################
### dir making
######################

$(EXTERNALS):
	@[ ! -d $(EXTERNALS) ] && mkdir $(EXTERNALS) || echo "" >/dev/null

$(RELEASES):
	@[ ! -d $(RELEASES) ] && mkdir $(RELEASES) || echo "" >/dev/null;


######################
### fetch dependencies
######################

# IMGUI
$(LIB_IMGUI): $(SO_IMGUI)
	@cp $(SO_IMGUI) $(LIB_IMGUI)

$(SO_IMGUI): $(REPO_IMGUI)
	@cd $(REPO_IMGUI) && \
		[ ! -d build ] && mkdir build || echo "" >/dev/null && \
		cd build && \
		cmake .. && make

$(REPO_IMGUI):
	git clone $(REMOTE_IMGUI) $(REPO_IMGUI)


# DKJSON
$(LIB_DKJSON):
	wget -O $(LIB_DKJSON) -- http://dkolf.de/src/dkjson-lua.fsl/raw/dkjson.lua?name=16cbc26080996d9da827df42cb0844a25518eeb3


# HUMP
$(LIB_HUMP): $(REPO_HUMP)
	@cp -r $(REPO_HUMP) $(LIB_HUMP)

$(REPO_HUMP):
	git clone $(REMOTE_HUMP) $(REPO_HUMP)


# CPML
$(LIB_CPML): $(REPO_CPML)
	@cp -r $(REPO_CPML) $(LIB_CPML)

$(REPO_CPML):
	git clone $(REMOTE_CPML) $(REPO_CPML)


# ACL2D
$(LIB_ACL2D): $(REPO_ACL2D)
	@cp -r $(REPO_ACL2D) $(LIB_ACL2D)

$(REPO_ACL2D):
	git clone $(REMOTE_ACL2D) $(REPO_ACL2D)



######################
### release
######################

$(RELEASE_BUNDLE): $(RELEASES)
	@cp -r $(GAME) $(RELEASES)/_temp
	@cd $(RELEASES)/_temp && \
		zip -9r ../game.love * -x *.git*
	@rm -rf $(RELEASES)/_temp

