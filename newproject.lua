
--[[ DEBUGGERS ]]--

local unpack = unpack or table.unpack
local function _log(...)
  local args = {...}
  print(unpack(args))
  return unpack(args)
end

--[[ ARGS ]]--

local source, target = arg[0]:match("(.*)/newproject.lua") or ".", arg[1]
local flags = setmetatable(
  {},
  {
    rawflags = table.pack(unpack(arg, 2)),
    __index = function(self, flagname)
      for _,raw in ipairs(getmetatable(self).rawflags) do
        if raw:match("^[-][-]"..flagname.."$") then
          return true
        end
      end
      return false
    end
  }
)


--[[ CONSTS ]]--

local _PATHS = {
  TEMPLATE = "/game",
  META = "/meta",
}

_IGNORE = "/.gitignore"
_ENGINE_LIB_NAME = "engine\n"
_WILDCARD = "/*"

local PATH_TO = setmetatable({}, {
  __index = function(self, k)
    return source..(_PATHS[k] or k)
  end
})

--[[ STEPS ]]--

local function _noTarget()
  return error(
    "No target directory.\nUsage:\n>> lua newproject.lua [targetdir]"
  )
end

local function _invalidTarget(target)
  return error(
    ("Target path `%s`\nis not a valid directory name."):format(target)
  )
end

local function _checkTargetDir(target)
  if flags.nuke then
    local confirm
    while not confirm do
      io.write(
        ("Are you sure you want to nuke `%s/`? [y/n]\n"):format(target)
      )
      local answer = io.read()
      if not answer then break end
      confirm = (answer:match("^[yY][Ee]?[Ss]?$") and 1)
                or (answer:match("^[nN][Oo]?$") and 0)
      if not confirm then print("Didn't catch that;") end
    end
    if not confirm or confirm == 0 then
      return false, _log("Won't nuke, quitting...")
    elseif confirm == 1 then
      os.execute(_log(("rm -rf %s/"):format(target)))
    end
  end

  local testfilepath = target.."/testing"
  local file = io.open(testfilepath, "w")
  return not not file
         and (
           file:close() and os.remove(testfilepath)
         )
         or (
           os.execute(_log(("mkdir %s"):format(target)))
           or _invalidTarget(target)
         )
end

local function _copyMetaTo(target)
  return os.execute(
    _log(("cp -v %s%s %s/"):format(PATH_TO.META, _WILDCARD, target))
  ) and os.execute(
    _log(("cp -v %s%s %s/"):format(PATH_TO.META, _IGNORE, target))
  )
end

local function _copyTemplateTo(target)
  return os.execute(
    _log(("cp -Rv %s %s/"):format(PATH_TO.TEMPLATE, target))
  )
end

local function _copyAllTo(target)
  return _copyMetaTo(target) and _copyTemplateTo(target)
end


--[[ RUN ]]--

local function _run(target)
  -- if no target, return, exit
  return not target and _noTarget() or
    (_checkTargetDir(target) and _copyAllTo(target))
end

local _success = _run(target)

if _success and flags.run then
  os.execute(
    _log(("cd %s && make"):format(target))
  )
end

return _success

